import 'package:flutter/material.dart';

import 'package:firebase_core/firebase_core.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import './ui_data/page/main_home_page.dart';
import './ui_data/page/location/location_page.dart';
import './ui_data/page/location/location_explore_buildings_page.dart';
import './logic_data/generalAppInfo/theming/data/app_theme.dart';
import './logic_data/generalAppInfo/theming/page/theme_page.dart';

void main() async {
  //Initialise Firebase on the app
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  //Initialise Hive local database on Flutter
  await Hive.initFlutter();
  runApp(ProviderScope(child: MyApp()));
}

// //Access the Color Theme Provider
// final customThemeProvider =
//     StateNotifierProvider<CustomColorTheme, ThemeData>((ref) {
//   return CustomColorTheme();
// });
// //Access the TextTheme Provider
// final customTextThemeProvider =
//     StateNotifierProvider<CustomTextTheme, TextTheme>((ref) {
//   return CustomTextTheme();
// });

class MyApp extends ConsumerWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    //Fetch and monitor the ThemeData

    //final ThemeData _customColorTheme = watch(customColorThemeProvider);
    final ThemeData _customTheme = ref.watch(customThemeProvider);

    return MaterialApp(
      title: 'Flutter Demo de François',
      //Reincorporate Theme
      theme: _customTheme,

      //List all the main routes
      routes: {
        LocationHomePage.routeName: (context) => LocationHomePage(),
        LocationExploreBuildingsPage.routeName: (context) =>
            LocationExploreBuildingsPage(),
        ThemePage.routeName: (context) => ThemePage(),
      },
      onUnknownRoute: (RouteSettings settings) {
        return MaterialPageRoute<void>(
          settings: settings,
          builder: (BuildContext context) =>
              Scaffold(body: Center(child: Text('Not Found'))),
        );
      },
      home: MyHomePage(title: 'Apollinis Pallatini'),
    );
  }
}
