import 'package:hive/hive.dart';
import 'dart:convert';

part 'building.g.dart';

//The building defines the physical location of an organisation
@HiveType(typeId: 1)
class Building {
  @HiveField(0)
  var id;
  @HiveField(1)
  final String title;
  @HiveField(2)
  final String address;
  @HiveField(3)
  final String postalCode;
  @HiveField(4)
  final String city;
  @HiveField(5)
  final String country;
  @HiveField(6)
  late final List<int> floors;
  @HiveField(7)
  final String description;

  Building({
    required this.id,
    required this.title,
    required this.floors,
    this.address = "",
    this.postalCode = "",
    this.city = "",
    this.country = "",
    this.description = "",
  });

  List<String> getFloorList() {
    List<String> namedList = [];
    for (int intFloor in floors) {
      namedList = [...namedList, "Floor $intFloor"];
    }
    return namedList;
  }

  //NEED TO TEST THE BUILT IN FUNCTIONS FOR JSON PARSING
  String buildingToJson2(Building building) => jsonEncode(building);

  Building buildingFromJson2(String json) => jsonDecode(json);

  Map<String, Object> buildingToJson() => <String, Object>{
        "title": title,
        "floors": floors,
        "address": address,
        "postalCode": postalCode,
        "city": city,
        "country": country,
        "description": description
      };

  Building buildingFromJson(Map<String, Object> json) {
    return Building(
      id: json['id'] as String,
      title: json['title'] as String,
      floors: json['floors'] as List<int>,
      address: json['address'] as String,
      postalCode: json['postalCode'] as String,
      city: json['city'] as String,
      country: json['country'] as String,
      description: json['description'] as String,
    );
  }
}
