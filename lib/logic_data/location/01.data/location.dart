import 'package:hive/hive.dart';

import 'business_unit.dart';
import './department.dart';
import './role.dart';

part 'location.g.dart';

//Location is the data that define a specific location within an organisation
//It serves as a locating point to list equipment into.

//Each location hold a number of arguments to define either its physical location
//or the responsibility the area falls into.

//The type is created with Hive annotations to enable
//the Typeadapter generator.
@HiveType(typeId: 0)
class Location {
  @HiveField(0)
  final String id;
  @HiveField(1)
  final String title;
  @HiveField(2)
  final String description;
  @HiveField(3)
  final BusinessUnit businessUnit;
  @HiveField(4)
  final Department department;
  @HiveField(5)
  final Role role;
  @HiveField(6)
  final Location? parentLocation;
  @HiveField(7)
  final List<Location>? childLocation;

  Location({
    required this.id,
    required this.businessUnit,
    required this.department,
    required this.role,
    required this.title,
    this.description = "",
    this.parentLocation,
    this.childLocation,
  });
}
