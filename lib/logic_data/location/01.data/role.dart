
//The role list defines all the acting functions in the business
class Role {
  final String role;
  final String description;

  Role(this.role, {this.description = ""});
}