//The Business Unit define a main aspect of the organisation,
//this mainly depends on the business organisation.
//It consists of a simple title and description.
class BusinessUnit {
  final String title;
  final String description;

  BusinessUnit(this.title, {this.description = ""});
}

