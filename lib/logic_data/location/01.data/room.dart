import 'package:hive/hive.dart';

part 'room.g.dart';

//The Room is a sub-class of the Area, and specifies
//a physical location within the area
@HiveType(typeId: 3)
class Room {
  @HiveField(0)
  final String id;
  @HiveField(1)
  final String title;
  @HiveField(2)
  final String description;
  @HiveField(3)
  final String areaID;
  @HiveField(4)
  final List<String>? locationsIDs;

  Room({
    required this.id,
    required this.title,
    required this.areaID,
    this.description = "",
    this.locationsIDs,
  });
}
