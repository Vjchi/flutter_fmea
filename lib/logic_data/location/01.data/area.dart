import 'package:hive/hive.dart';

part 'area.g.dart';

//The Area is a sub-class of the floor,
//and defines sections within

@HiveType(typeId: 2)
class Area {
  @HiveField(0)
  final String id;
  @HiveField(1)
  final String title;
  @HiveField(2)
  final String description;
  @HiveField(3)
  final String buildingID;
  @HiveField(4)
  final int floor;
  @HiveField(5)
  final List<String>? rooms;

  Area({
    //required this.id,
    required this.id,
    required this.title,
    required this.buildingID,
    required this.floor,
    this.rooms,
    this.description = "",
  });
}
