//The department list defines different sub-classes of the BU
class Department {
  final String department;
  final String description;

  Department({required this.department, this.description = ""});
}
