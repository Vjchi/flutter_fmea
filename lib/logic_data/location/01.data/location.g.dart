// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'location.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class LocationAdapter extends TypeAdapter<Location> {
  @override
  final int typeId = 0;

  @override
  Location read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Location(
      id: fields[0] as String,
      businessUnit: fields[3] as BusinessUnit,
      department: fields[4] as Department,
      role: fields[5] as Role,
      title: fields[1] as String,
      description: fields[2] as String,
      parentLocation: fields[6] as Location?,
      childLocation: (fields[7] as List?)?.cast<Location>(),
    );
  }

  @override
  void write(BinaryWriter writer, Location obj) {
    writer
      ..writeByte(8)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.title)
      ..writeByte(2)
      ..write(obj.description)
      ..writeByte(3)
      ..write(obj.businessUnit)
      ..writeByte(4)
      ..write(obj.department)
      ..writeByte(5)
      ..write(obj.role)
      ..writeByte(6)
      ..write(obj.parentLocation)
      ..writeByte(7)
      ..write(obj.childLocation);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is LocationAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
