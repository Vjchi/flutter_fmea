import 'package:cloud_firestore/cloud_firestore.dart';
import '../04.framework/building_firestore_handler.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../01.data/building.dart';

//The Use-case covers all the company rules associated with the item
//as well as the provider of the item for the app.

//The building defines the physical location of an organisation,
//that can be defined by a postal address

//Building are used as StateNotifier with Riverpod
//StateNotifier
class BuildingsState extends StateNotifier<List<Building>> {
  BuildingsState([List<Building>? _buildings])
      : super(_buildings ??
            [
              Building(
                  id: "200721-BCMNotts#",
                  title: "D10 - BCM Fareva",
                  address: "20 Thane Road",
                  city: "Nottingham",
                  country: "United Kingdom",
                  postalCode: "NG9 2PR",
                  description:
                      "This is the Nottingham based business that used to be Boots Manufacturing and Packing plant.",
                  floors: [-1, 0, 1]),
              Building(
                  id: "200721BCMVitré",
                  title: "BCM Vitré",
                  address: "34 Route des Eaux",
                  city: "Vitré",
                  postalCode: "35500",
                  country: "France",
                  description:
                      "This is the French site that used to be part of BCM group.",
                  floors: [0, 1]),
            ]);

  List<Building> get() {
    final List<Building> buildings = state;
    return buildings;
  }

  bool isExisting(Building newBuilding) {
    for (Building building in state) {
      //Before adding a building we make sure we don't already have one
      //with the same name and same city
      if (building.title == newBuilding.title) {
        //if it already exists, send signal
        print("Building already exists, cannot add.");
        return true;
      }
    }
    return false;
  }

  void add(Building newBuilding) async {
    //ADD VALIDATION AND CONTROL

    if (!isExisting(newBuilding)) {
      //Access Riverpod Providers
      final riverpodContainer = ProviderContainer();
      //Add the building on Repository
      final String idRef = await riverpodContainer
          .read(buildingRepositoryProvider)
          .add(newBuilding.buildingToJson());

      //Update building ID with the provided ID
      newBuilding.id = idRef;

      state = [...state, newBuilding];

      print("building added!");
    } else {
      print("building already exists");
    }
    return;
  }

  void removeBuilding(String _id) {
    //ADD VALIDATION AND CONTROL
    state.removeWhere((element) => element.id == _id);
    //Access Riverpod providers
    final riverpodContainer = ProviderContainer();
    //Remove building from Repository using id
    riverpodContainer.read(buildingRepositoryProvider).remove(_id);
    //Call state to update notifiers
    state = state;
    print("remove $_id");
    return;
  }

  List<int> getFloors(Building building) {
    //Fetch the first building where the ID matches
    //Returns the list of floors associated
    return state.firstWhere((element) => element.id == building.id).floors;
  }

  List<int> createFloorList(int low, int top) {
    //Return the list of floors of a building
    List<int> _list = [];
    for (var index = low; index <= top; index++) {
      _list = [..._list, index];
    }
    return _list;
  }
}

//We create the provider that will tie in with the Notifier
//This allow to access the state of the notifier from
//anywhere in the app using Riverpod
final buildingsProvider = StateNotifierProvider<BuildingsState, List<Building>>(
    (ref) => BuildingsState());
