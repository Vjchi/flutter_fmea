import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../01.data/department.dart';
import '../01.data/business_unit.dart';

//The department list defines different sub-classes of the BU
//It is based on the BU itself
//As such, it is a list of Attibutes mapped to a specific BU.
//e.g. for Food : Sourcing, Storage, Processing, Packing, etc.

class Departments extends StateNotifier<Map<BusinessUnit, List<Department>>> {
  Departments([Map<BusinessUnit, List<Department>>? _departments])
      : super(_departments ?? {});

  //When using Riverpod StateNotifier, the return object
  //is always marked down as 'state'
  get getDepartments {
    return state;
  }
}
