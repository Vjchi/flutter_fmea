import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../01.data/room.dart';

//The Room is a sub-class of the Area, and specifies
//a physical location within the area
//e.g. Room 314, Meeting Room, Packing Line 42, etc.

class RoomsState extends StateNotifier<List<Room>?> {
  RoomsState([List<Room>? _rooms])
      : super(_rooms ??
            [
              Room(
                id: "room_test1",
                title: "Bay 01",
                areaID: "area-test4",
                locationsIDs: ["01-bay01-line08"],
              ),
              Room(id: "room_test2a", title: "Bay 02A", areaID: "area-test4"),
              Room(id: "room_test2b", title: "Bay 02B", areaID: "area-test4"),
              Room(id: "room_test3", title: "Bay 03", areaID: "area-test4"),
              Room(id: "room_test4", title: "Bay 04", areaID: "area-test4"),
              Room(id: "room_test5", title: "Bay 05", areaID: "area-test4"),
            ]);

//Getter to get the rooms
  List<Room> get getRooms {
    return [...state!];
  }

//Add a room to the register
//!\ REQUIRES ADDING CHECK FOR EXISTING
//!\ REQUIRES ADDING CACHE UPDATE
//!\ REQUIRES ADDING ONLINE UPDATE
  void addRoom(Room room) {
    state?.add(room);
  }

  List<Room> getRoomsFromArea(List<String> roomListID) {
    List<Room> roomList;
    roomList = [];
    //If there is no rooms in the state
    if (state == null) {
      return roomList;
    }
    //Go through list of IDs and fetch relevant rooms
    for (String roomID in roomListID) {
      roomList.add(state!.firstWhere((room) => room.id == roomID));
    }
    return roomList;
  }
}

//Access the room provider
//!\ REQUIRES ADDING CACHE UPDATE
final roomsProvider = StateNotifierProvider<RoomsState, List<Room>?>((ref) {
  return RoomsState();
});
