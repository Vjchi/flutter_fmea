import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../01.data/business_unit.dart';

//The BU list is the list of all Business Units in the organisation
//This can widely differ depending on the organisation.
//It's mainly here as a first differenciation of the business entities.
//e.g. Food , Cosmetic, Pharmaceutical, Chemical, etc.
//Two Business Units cannot share the same name.
class BusinessUnits extends StateNotifier {
  BusinessUnits([List<BusinessUnit>? _buList]) : super(_buList ?? []);
  
  //When using Riverpod StateNotifier, the return object
  //is always marked down as 'state'
  List<BusinessUnit> get buList {
    return [...state];
  }

  //add business unit to the list, but
  //check for doubles first
  void addBU(BusinessUnit bu) {
    List<String> buTitleList = [];
    state.forEach((item) {
      buTitleList.add(item.title);
    });
    buTitleList.forEach((element) {
      if (bu.title == element) {
        return;
      } else {
        state.add(bu);
        return;
      }
    });
  }
}

