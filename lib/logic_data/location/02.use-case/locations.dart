
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:hive/hive.dart';

import '../01.data/business_unit.dart';
import '../01.data/department.dart';
import '../01.data/location.dart';
import '../01.data/role.dart';

class LocationsState extends StateNotifier<List<Location>> {
//The list of location is the temporary location of all
//the locations in the program.
//It's reset everytime the program is stoppped.
  LocationsState([List<Location>? _locations])
      : super(_locations ??
            [
              Location(
                  id: "01-bay01-line08",
                  title: "Line 08",
                  businessUnit: BusinessUnit("Pharma"),
                  department: Department(department: "Operations"),
                  role: Role("BOM"))
            ]);

  //The permanent data is saved using the Hive in-memory database
  //Everytime a service is called, the Hive box needs to be opened.
  Future<Box> openHiveLocationsBox() async {
    //!\ NOT SURE THIS WORKS /!\\
    return await Hive.openBox("locationBox");
  }

  List<Location> get getLocations {
    return [...state];
  }

  //!\NOT SURE IT WORKS
  addLocation(Location loc) async {
    try {
      var box = await Hive.openBox<Location>("locationBox");
      state.add(loc);
      box.add(loc);
    } catch (error) {
      throw error;
    }
  }

//!\NOT SURE IT WORKS
  void removeLocation(Location loc) async {
    try {
      var box = await Hive.openBox<Location>("locationBox");
      box.delete(loc);
      state.remove(loc);
    } catch (error) {
      throw error;
    }
  }
}

//Access the locations Provider
//Requires adding cache support
final locationsProvider =
    StateNotifierProvider<LocationsState, List<Location>>((ref) {
  return LocationsState();
});

//The role list defines all the acting functions in the business
//Contrary to BU and department, it focuses on the roles of people,
//As such, it is considered business-wide, and therefore requires
//careful considerations when being defined
//e.g. Operations, Quality, Engineering, Finances, etc.
class Roles {
  List<Role> _roleList = [];

  get roleList {
    return _roleList;
  }
}


//TO-DO:
//Add function to store new locations on Firestore
//Add function to remove location from Firestore
//Add function to update location from Firestore
//Add function to locally manage through SQLite