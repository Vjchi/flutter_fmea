import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../01.data/area.dart';

//The Area is a sub-class of the floor, and defines sections within
//It is mapped on the id of the building,
// and then on floor, and consists of a list of String.
class AreasState extends StateNotifier<List<Area>?> {
  AreasState([List<Area>? _areas])
      : super(_areas ??
            [
              Area(
                  id: "area-test1",
                  title: "Offices",
                  buildingID: "200721-BCMNotts#",
                  floor: 1),
              Area(
                  id: "area-test2",
                  title: "Pharma Dispensary",
                  buildingID: "200721-BCMNotts#",
                  floor: 1),
              Area(
                  id: "area-test3",
                  title: "Raw Material Storage",
                  buildingID: "200721-BCMNotts#",
                  floor: 1),
              Area(
                  id: "area-test4",
                  title: "Pharma Packing",
                  buildingID: "200721-BCMNotts#",
                  floor: 0,
                  rooms: [
                    "room_test1",
                    "room_test2a",
                    "room_test2b",
                    "room_test3",
                    "room_test4",
                    "room_test5"
                  ]),
              Area(
                  id: "area-test5",
                  title: "Beauty 01 Packing",
                  buildingID: "200721-BCMNotts#",
                  floor: 0),
              Area(
                  id: "area-test6",
                  title: "Beauty 03 Packing",
                  buildingID: "200721-BCMNotts#",
                  floor: 0),
              Area(
                  id: "area-test7",
                  title: "Beauty 02 Packing",
                  buildingID: "200721-BCMNotts#",
                  floor: 0),
              Area(
                  id: "area-test8",
                  title: "Beauty 03 Manufacturing",
                  buildingID: "200721-BCMNotts#",
                  floor: 0),
              Area(
                  id: "area-test9",
                  title: "Beauty 02 Manufacturing",
                  buildingID: "200721-BCMNotts#",
                  floor: 0),
              Area(
                  id: "area-test10",
                  title: "Pharma Storge",
                  buildingID: "200721-BCMNotts#",
                  floor: -1),
              Area(
                  id: "area-test11",
                  title: "Critical Plant",
                  buildingID: "200721-BCMNotts#",
                  floor: -1),
              Area(
                  id: "area-test12",
                  title: "Beauty Storage",
                  buildingID: "200721-BCMNotts#",
                  floor: -1),
              Area(
                  id: "area-test13",
                  title: "Beauty Packing",
                  buildingID: "200721BCMVitré",
                  floor: 0),
              Area(
                  id: "area-test14",
                  title: "Beauty Manufacturing",
                  buildingID: "200721BCMVitré",
                  floor: 0),
              Area(
                  id: "area-test15",
                  title: "Accounting",
                  buildingID: "200721BCMVitré",
                  floor: 0),
              Area(
                  id: "area-test16",
                  title: "Baguette Research",
                  buildingID: "200721BCMVitré",
                  floor: 0),
              Area(
                  id: "area-test17",
                  title: "Le storage vessels",
                  buildingID: "200721BCMVitré",
                  floor: 1),
              Area(
                  id: "area-test18",
                  title: "Critical Plant",
                  buildingID: "200721BCMVitré",
                  floor: 1),
              Area(
                  id: "area-test19",
                  title: "Surdough Secret Laboratory",
                  buildingID: "200721BCMVitré",
                  floor: 1),
              Area(
                  id: "area-test20",
                  title: "Purified Water",
                  buildingID: "200721BCMVitré",
                  floor: 1),
            ]);

  //When using Riverpod StateNotifier, the return object
  //is always marked down as 'state'
  List<Area>? getAreas(String buildingID) {
    final List<Area>? _listAreas =
        state!.where((area) => (area.buildingID == buildingID)).toList();
    return _listAreas ?? [];
  }

  void addArea(String buildingID, int floor, Area area) {
    state?.add(area);
  }
}

//Access the areas provider
final areasProvider = StateNotifierProvider<AreasState, List<Area>?>((ref) {
  return AreasState();
});
