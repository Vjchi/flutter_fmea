import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:riverpod/riverpod.dart';

import '../02.use-case/buildings.dart';

class BuildingRepositoryState {
  Future<String> add(Map<String, Object> jsonBuilding) async {
    DocumentReference documentRef = await FirebaseFirestore.instance
        .collection('buildings')
        .add(jsonBuilding);
    //PRINT TEST
    print(jsonBuilding);
    return documentRef.id;
  }

  void remove(String id) {
    FirebaseFirestore.instance.collection('buildings').doc(id).delete();
  }
}

//Open the Provider to the rest of the app
final buildingRepositoryProvider =
    Provider<BuildingRepositoryState>((ref) => BuildingRepositoryState());
