import 'package:flutter/material.dart';

import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../ui_data/page/location/location_page.dart';

class HomePageRoutingItem {
  HomePageRoutingItem({
    required this.title,
    required this.route,
    required this.icon,
  });

  final String title;
  final String route;
  final Icon icon;
}

final getHomePageRoutingItemList = Provider((ref) => _homePageRoutingList);

List<HomePageRoutingItem> _homePageRoutingList = [
  HomePageRoutingItem(
      icon: Icon(Icons.store),
      title: "Locations",
      route: LocationHomePage.routeName),
  HomePageRoutingItem(
    icon: Icon(Icons.build),
    title: "Assets",
    route: "/assetsHomePage",
  ),
  HomePageRoutingItem(
      icon: Icon(Icons.import_contacts),
      title: "Documents",
      route: "/documentsHomePage"),
  HomePageRoutingItem(
    icon: Icon(Icons.ballot_rounded),
    title: "FMEA",
    route: "/fmeaHomePage",
  ),
];
