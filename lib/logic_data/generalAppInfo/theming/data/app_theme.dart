import 'package:flex_color_scheme/flex_color_scheme.dart';
import 'package:flutter/material.dart';
import 'package:riverpod/riverpod.dart';

import './color_theme.dart';
import './text_theme.dart';
import './light_mode_theme.dart';

final customThemeProvider = Provider<ThemeData>((ref) {
  //Fetch the TextScheme and the ColorScheme

  final FlexScheme _colorTheme = ref.watch(customColorThemeProvider);
  final TextTheme _textTheme = ref.watch(customTextThemeProvider);
  final bool _lightTheme = ref.watch(customLightThemeProvider);

//Build the final Scheme from it
  final ThemeData _customTheme;
  if (_lightTheme == true) {
    _customTheme =
        FlexColorScheme.light(scheme: _colorTheme, textTheme: _textTheme)
            .toTheme;
  } else {
    _customTheme =
        FlexColorScheme.dark(scheme: _colorTheme, textTheme: _textTheme)
            .toTheme;
  }
  return _customTheme;
  //return _colorTheme.copyWith(textTheme: _textTheme);
  //ISSUE: CopyWith overrides color of the text, previous line doesn't work
});
