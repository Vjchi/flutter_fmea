//import 'package:google_fonts/google_fonts.dart';
import 'package:flex_color_scheme/flex_color_scheme.dart';
import 'package:flutter/cupertino.dart';
import 'package:riverpod/riverpod.dart';

const List<FlexScheme> _listScheme = [
  FlexScheme.mandyRed,
  FlexScheme.outerSpace,
  FlexScheme.green,
  FlexScheme.deepPurple,
  FlexScheme.aquaBlue,
  FlexScheme.bigStone,
  FlexScheme.espresso,
  FlexScheme.gold,
];

//CustomColorTheme contains the state of the Theme
class CustomColorTheme extends StateNotifier<FlexScheme> {
  var _indexColor = 0;

  CustomColorTheme()
      : super(
          FlexScheme.mandyRed,
        );

  void switchColor() {
    _indexColor < _listScheme.length-1 ? _indexColor++ : _indexColor = 0;
    state = _listScheme[_indexColor % _listScheme.length];
    print("The Color scheme has been changed to : ${_listScheme[_indexColor]}");
  }

  //Get the colors
  List<List<Color>> getColorList() {
    List<List<Color>> colorList = [[]]; //ISSUE AROUND HERE !!!
    List<FlexScheme> flexList = _listScheme;

    for (int i = 0; i < flexList.length; i++) {
      colorList[i][0] = FlexColorScheme.light(scheme: flexList[i]).primary;
      colorList[i][1] = FlexColorScheme.light(scheme: flexList[i]).secondary;
    }
    return colorList;
  }
}

//Access the Color Theme Provider
final customColorThemeProvider =
    StateNotifierProvider<CustomColorTheme, FlexScheme>((ref) {
  return CustomColorTheme();
});
