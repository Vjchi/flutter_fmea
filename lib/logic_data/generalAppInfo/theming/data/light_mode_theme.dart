import 'package:riverpod/riverpod.dart';

class LightMode extends StateNotifier<bool> {
  LightMode() : super(true);

  void switchLightMode(bool newValue) {
    state = newValue;
    print("Light Mode activated is $state");
  }

  bool getState() {
    return state;
  }
}

final customLightThemeProvider = StateNotifierProvider<LightMode, bool>((ref) {
  return LightMode();
});
