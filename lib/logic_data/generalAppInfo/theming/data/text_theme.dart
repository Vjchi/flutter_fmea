import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_fonts/google_fonts.dart';

class CustomTextTheme extends StateNotifier<TextTheme> {
  CustomTextTheme()
      : super(TextTheme(
            headline1: GoogleFonts.notoSans(
              textStyle: TextStyle(
                  fontSize: 96,
                  fontWeight: FontWeight.w300,
                  letterSpacing: -1.5),
            ),
            headline2: GoogleFonts.notoSans(
              textStyle: TextStyle(
                  fontSize: 60,
                  fontWeight: FontWeight.w300,
                  letterSpacing: -0.5),
            ),
            headline3: GoogleFonts.notoSans(
              textStyle: TextStyle(fontSize: 48, fontWeight: FontWeight.w400),
            ),
            headline4: GoogleFonts.notoSans(
              textStyle: TextStyle(
                  fontSize: 34,
                  fontWeight: FontWeight.w400,
                  letterSpacing: 0.25),
            ),
            headline5: GoogleFonts.notoSans(
              textStyle: TextStyle(fontSize: 24, fontWeight: FontWeight.w400),
            ),
            headline6: GoogleFonts.notoSans(
              textStyle: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
              letterSpacing: 0.15,
            ),
            bodyText1: GoogleFonts.lato(
                textStyle: TextStyle(
              fontSize: 16,
              letterSpacing: 0.5,
            )),
            bodyText2: GoogleFonts.lato(
              textStyle: TextStyle(
                fontSize: 14,
                letterSpacing: 0.25,
              ),
            )));

  void toggleAccessibleText(bool isAccessible) {
    isAccessible ? state = accessibleText : state = defaultText;
    print("TextTheme accessibility mode: $isAccessible");
    print("The font is ${state.headline1}");
  }

//Sample default theme
  final TextTheme defaultText = TextTheme(
      headline1: GoogleFonts.lobster(
        textStyle: TextStyle(
            fontSize: 96, fontWeight: FontWeight.w300, letterSpacing: -1.5),
      ),
      headline2: GoogleFonts.lobster(
        textStyle: TextStyle(
            fontSize: 60, fontWeight: FontWeight.w300, letterSpacing: -0.5),
      ),
      headline3: GoogleFonts.lobster(
        textStyle: TextStyle(fontSize: 48, fontWeight: FontWeight.w400),
      ),
      headline4: GoogleFonts.lobster(
        textStyle: TextStyle(
            fontSize: 34, fontWeight: FontWeight.w400, letterSpacing: 0.25),
      ),
      headline5: GoogleFonts.lobster(
        textStyle: TextStyle(fontSize: 24, fontWeight: FontWeight.w400),
      ),
      headline6: GoogleFonts.lobster(
        textStyle: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
        letterSpacing: 0.15,
      ),
      bodyText1: GoogleFonts.lobsterTwo(
          textStyle: TextStyle(
        fontSize: 16,
        letterSpacing: 0.5,
      )),
      bodyText2: GoogleFonts.lobsterTwo(
        textStyle: TextStyle(
          fontSize: 14,
          letterSpacing: 0.25,
        ),
      ));

//Sample TextTheme
  final TextTheme accessibleText = TextTheme(
      headline1: GoogleFonts.notoSans(
        textStyle: TextStyle(
            fontSize: 96, fontWeight: FontWeight.w300, letterSpacing: -1.5),
      ),
      headline2: GoogleFonts.notoSans(
        textStyle: TextStyle(
            fontSize: 60, fontWeight: FontWeight.w300, letterSpacing: -0.5),
      ),
      headline3: GoogleFonts.notoSans(
        textStyle: TextStyle(fontSize: 48, fontWeight: FontWeight.w400),
      ),
      headline4: GoogleFonts.notoSans(
        textStyle: TextStyle(
            fontSize: 34, fontWeight: FontWeight.w400, letterSpacing: 0.25),
      ),
      headline5: GoogleFonts.notoSans(
        textStyle: TextStyle(fontSize: 24, fontWeight: FontWeight.w400),
      ),
      headline6: GoogleFonts.notoSans(
        textStyle: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
        letterSpacing: 0.15,
      ),
      bodyText1: GoogleFonts.openSans(
          textStyle: TextStyle(
        fontSize: 16,
        letterSpacing: 0.5,
      )),
      bodyText2: GoogleFonts.openSans(
        textStyle: TextStyle(
          fontSize: 14,
          letterSpacing: 0.25,
        ),
      ));
}

//Access the TextTheme Provider
final customTextThemeProvider =
    StateNotifierProvider<CustomTextTheme, TextTheme>((ref) {
  return CustomTextTheme();
});
