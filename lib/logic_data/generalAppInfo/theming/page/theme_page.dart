import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

//import '../../theming/data/color_theme.dart';

class ThemePage extends HookConsumerWidget {
  const ThemePage({Key? key}) : super(key: key);
  static const String routeName = "ThemePage";

  @override
  Widget build(BuildContext context, WidgetRef ref) {

    //Access the Theme color Provider
    //var customColorProvider = ref.watch(customColorThemeProvider.notifier);
    //List<List<Color>> _colorList = customColorProvider.getColorList();

    return Scaffold(
      appBar: AppBar(
        title: Text("Theme"),
      ),
      body: SafeArea(
        child: Container(
          
          child: SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.all(8),
                  width: double.infinity,
                  child: Text(
                    "Colours Selection",
                    style: Theme.of(context).textTheme.headline6,
                  ),
                ),
                GridView.count(
                  children: [
                    Chip(label: Text("Essai 1")),
                    Chip(label: Text("Essai 2")),
                    Chip(label: Text("Essai 3")),
                    Chip(label: Text("Essai 4")),
                    Chip(label: Text("Essai 5")),
                  ],
                  crossAxisCount: 8,
                ),
                Container(
                  margin: EdgeInsets.all(8),
                  width: double.infinity,
                  child: Text(
                    "Light Mode",
                    style: Theme.of(context).textTheme.headline6,
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(8),
                  width: double.infinity,
                  child: Text(
                    "Text Customization",
                    style: Theme.of(context).textTheme.headline6,
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
