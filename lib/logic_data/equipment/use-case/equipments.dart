import '../data/equipment.dart';

//The Equipments serve as an entry point to manage
//the list of equipment in temporary memory.
class Equipments {
  List<Equipment> _equipmentList = [];

  void addEquipment(Equipment equipment) {
    _equipmentList.add(equipment);
    return;
  }

}