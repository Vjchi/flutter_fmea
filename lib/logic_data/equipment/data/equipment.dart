//The Equipment class covers the existing equipment (assets)
//Equipment include a range of attributes to define
//the type of asset, as well as its location in the organisation

import 'package:hive/hive.dart';
@HiveType(typeId: 10)
class Equipment  {
  final String id;
  final String title;
  final String description;
  final String locationID;
  final String manufacturer;
  final String model;
  final String serialNumber;
  final List<Equipment>? subEquipment;

  Equipment(
    this.id,
    this.title,
    this.locationID,
    {this.description = "",
    this.manufacturer = "",
    this.model = "",
    this.serialNumber = "",
    this.subEquipment,
  });
}
