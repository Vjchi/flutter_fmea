class FmeaItem {
  final String id;
  final String failureMode;
  
  FmeaItem(this.id, this.failureMode);
}