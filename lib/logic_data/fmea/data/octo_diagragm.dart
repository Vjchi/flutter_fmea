enum elementClass {
  user,
  object,
  externals,
}

class ElementDiagram {
  final String title;
  final elementClass type;

  ElementDiagram(this.title, this.type);
}

enum functionClass {
  primary,
  secondary,
}

class FunctionDiagram {
  final String title;
  final functionClass;
  final String description;

  FunctionDiagram(this.title, this.functionClass, this.description);
}
