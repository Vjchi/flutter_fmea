import 'package:flutter/material.dart';

import '../../logic_data/generalAppInfo/routing/home_page_routing_item.dart';

class HomeGridTile extends StatelessWidget {
  const HomeGridTile({
    required this.item,
    Key? key,
  }) : super(key: key);

  final HomePageRoutingItem item;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => Navigator.of(context).pushNamed(
        item.route,
      ),
      child: Card(
        elevation: 4,
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              item.icon,
              Text(item.title),
            ],
          ),
        ),
      ),
    );
  }
}
