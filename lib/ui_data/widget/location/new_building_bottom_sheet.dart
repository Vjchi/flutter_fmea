import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:nanoid/non_secure.dart';

import '../../../logic_data/location/01.data/building.dart';
import '../../../logic_data/location/02.use-case/buildings.dart';

class NewBuildingBottomSheet extends HookConsumerWidget {
  const NewBuildingBottomSheet({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

    String _buildingName = "";
    String _buildingId = nanoid(10);
    String _buildingDescription = "";
    String _buildingAddress = "";
    String _buildingPostalCode = "";
    String _buildingCity = "";
    String _buildingCountry = "";
    int _lowFloor = ref.watch(_lowFloorProvider).state;
    int _topFloor = ref.watch(_topFloorProvider).state;

    TextEditingController _titleController = useTextEditingController();
    TextEditingController _descriptionController = useTextEditingController();
    TextEditingController _addressController = useTextEditingController();
    TextEditingController _postalCodeController = useTextEditingController();
    TextEditingController _cityController = useTextEditingController();
    TextEditingController _countryController = useTextEditingController();

    FocusNode _titleFocusNode = useFocusNode();
    FocusNode _descriptionFocusNode = useFocusNode();
    FocusNode _addressFocusNode = useFocusNode();
    FocusNode _postalCodeFocusNode = useFocusNode();
    FocusNode _cityFocusNode = useFocusNode();
    FocusNode _countryFocusNode = useFocusNode();

    return Stack(
      alignment: Alignment.topCenter,
      children: [
        Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(24),
              color: Theme.of(context).colorScheme.background),
          margin: EdgeInsets.only(top: 40),
          padding: EdgeInsets.only(top: 40, left: 10, right: 10, bottom: 20),
          child: Form(
            key: _formKey,
            child: Container(
              margin: EdgeInsets.all(12),
              child: SingleChildScrollView(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(
                        "New Building",
                        style: Theme.of(context).textTheme.headline4,
                      ),
                      SizedBox(height: 12),

                      //TEXTFIELD
                      //Title TextFormField
                      TextFormField(
                        key: Key("title"),
                        controller: _titleController,
                        focusNode: _titleFocusNode,
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        decoration: const InputDecoration(
                          fillColor: Colors.white10,
                          icon: Icon(Icons.store_mall_directory_outlined),
                          hintText: 'What do people call you?',
                          labelText: 'Building Name',
                        ),
                        validator: (String? value) {
                          return (value == "") ? "Please enter a name" : null;
                        },
                        onSaved: (String? value) => _buildingName = value ?? "",
                        onTap: () => _titleFocusNode.requestFocus(),
                      ),
                      SizedBox(
                        height: 24,
                      ),
                      NewBuildingFloorsSelection(),
                      SizedBox(
                        height: 24,
                      ),

                      //TEXTFIELD:
                      //Address TextFormField
                      TextFormField(
                        key: Key("address"),
                        controller: _addressController,
                        focusNode: _addressFocusNode,
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        decoration: const InputDecoration(
                          fillColor: Colors.white10,
                          icon: Icon(Icons.location_on_outlined),
                          hintText: 'Where are you located?',
                          labelText: 'Address',
                        ),
                        onSaved: (String? value) =>
                            _buildingAddress = value ?? "",
                        onFieldSubmitted: (String? value) =>
                            _cityFocusNode.requestFocus(),
                        onTap: () => _addressFocusNode.requestFocus(),
                      ),
                      SizedBox(
                        height: 12,
                      ),

                      //TEXTFIELD
                      //City TextFormField
                      TextFormField(
                        key: Key("city"),
                        controller: _cityController,
                        focusNode: _cityFocusNode,
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        decoration: const InputDecoration(
                          fillColor: Colors.white10,
                          icon: Icon(Icons.location_city_sharp),
                          hintText: 'What city are you in?',
                          labelText: 'City',
                        ),
                        validator: (String? value) {
                          return (value == "") ? "Please enter a city" : null;
                        },
                        onSaved: (String? value) => _buildingCity = value ?? "",
                        onFieldSubmitted: (String? value) =>
                            _postalCodeFocusNode.requestFocus(),
                        onTap: () => _cityFocusNode.requestFocus(),
                      ),
                      SizedBox(
                        height: 12,
                      ),

                      //TEXTFIELD
                      //PostalCode TextFormField
                      TextFormField(
                        key: Key("postalCode"),
                        controller: _postalCodeController,
                        focusNode: _postalCodeFocusNode,
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        decoration: const InputDecoration(
                          fillColor: Colors.white10,
                          icon: Icon(Icons.map_outlined),
                          hintText: 'What is your postal code?',
                          labelText: 'Postal Code',
                        ),
                        onSaved: (String? value) =>
                            _buildingPostalCode = value ?? "",
                        onFieldSubmitted: (String? value) =>
                            _countryFocusNode.requestFocus(),
                        onTap: () => _postalCodeFocusNode.requestFocus(),
                      ),
                      SizedBox(
                        height: 12,
                      ),

                      //TEXTFIELD
                      //Country TextFormField
                      TextFormField(
                        key: Key("country"),
                        controller: _countryController,
                        focusNode: _countryFocusNode,
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        decoration: const InputDecoration(
                          fillColor: Colors.white10,
                          icon: Icon(Icons.outlined_flag),
                          hintText: 'What country are you part of?',
                          labelText: 'Country',
                        ),
                        validator: (String? value) {
                          return (value == "")
                              ? "Please enter a Country"
                              : null;
                        },
                        onSaved: (String? value) =>
                            _buildingCountry = value ?? "",
                        onTap: () => _countryFocusNode.requestFocus(),
                      ),
                      SizedBox(
                        height: 12,
                      ),

                      //TEXTFIELD
                      //Description TextFormField
                      TextFormField(
                        key: Key("description"),
                        controller: _descriptionController,
                        focusNode: _descriptionFocusNode,
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        decoration: const InputDecoration(
                          fillColor: Colors.white10,
                          icon: Icon(Icons.store_mall_directory_outlined),
                          hintText: 'Add a description.',
                          labelText: 'Description',
                        ),
                        onSaved: (String? value) =>
                            _buildingDescription = value ?? "",
                        onTap: () => _descriptionFocusNode.requestFocus(),
                      ),
                      SizedBox(
                        height: 24,
                      ),

                      //Floors Selection Widget
                      Consumer(builder:
                          (BuildContext context, WidgetRef ref, Widget? child) {
                        BuildingsState buildings =
                            ref.watch(buildingsProvider.notifier);
                        return ElevatedButton(
                            child: Container(
                                alignment: Alignment.center,
                                width: double.infinity,
                                child: Text("Create")),
                            onPressed: () {
                              _formKey.currentState!.save();
                              print(_buildingName);
                              buildings.add(Building(
                                title: _buildingName,
                                floors: buildings.createFloorList(
                                    _lowFloor, _topFloor),
                                id: _buildingId,
                                description: _buildingDescription,
                                address: _buildingAddress,
                                city: _buildingCity,
                                postalCode: _buildingPostalCode,
                                country: _buildingCountry,
                              ));
                              ScaffoldMessenger.of(context).showSnackBar(
                                  SnackBar(
                                      content:
                                          Text("You added $_buildingName!")));
                              Navigator.of(context).pop();
                            });
                      }),
                    ]),
              ),
            ),
          ),
        ),
        CircleAvatar(
          backgroundColor: Theme.of(context).primaryColor,
          radius: 40,
          child: Icon(
            Icons.home_work_outlined,
            color: Theme.of(context).colorScheme.onPrimary,
          ),
        )
      ],
    );
  }
}

//Create the Riverpod providers for the floors
//Use autoDispose to destroy state when no longer used
final _topFloorProvider = StateProvider.autoDispose<int>((ref) => 0);
final _lowFloorProvider = StateProvider.autoDispose<int>((ref) => 0);

class NewBuildingFloorsSelection extends ConsumerWidget {
  NewBuildingFloorsSelection({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    int _lowFloor = ref.watch(_lowFloorProvider).state;
    int _topFloor = ref.watch(_topFloorProvider).state;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(
          "Enter Bottom and Top Floors",
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.headline6,
        ),
        Flex(
          direction: Axis.horizontal,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              padding: EdgeInsets.all(8),
              child: Text(
                "Bottom",
              ),
            ),
            Container(
              padding: EdgeInsets.all(8),
              child: Text(
                "Top",
              ),
            )
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            //SELECTION OF THE BOTTOM FLOOR
            Column(
              children: [
                IconButton(
                  onPressed: _lowFloor >= _topFloor
                      ? null
                      : () => ref.watch(_lowFloorProvider).state++,
                  icon: Icon(Icons.arrow_circle_up_outlined),
                ),
                IconButton(
                    onPressed: () {
                      ref.watch(_lowFloorProvider).state--;
                    },
                    icon: Icon(Icons.arrow_circle_down_outlined)),
              ],
            ),
            Text("$_lowFloor"),
            SizedBox(
              width: 16,
            ),
            //SELECTION OF THE TOP FLOOR
            Text("$_topFloor"),
            Column(
              children: [
                IconButton(
                  onPressed: () => ref.watch(_topFloorProvider).state++,
                  icon: Icon(Icons.arrow_circle_up_outlined),
                ),
                IconButton(
                    onPressed: _topFloor <= _lowFloor
                        ? null
                        : () =>
                            //If top floor is above low, we can decrease it
                            ref.watch(_topFloorProvider).state--,
                    icon: Icon(Icons.arrow_circle_down_outlined)),
              ],
            ),
          ],
        ),
      ],
    );
  }
}
