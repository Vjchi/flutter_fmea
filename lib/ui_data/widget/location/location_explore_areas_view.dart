import 'package:flutter/material.dart';

import '../../../logic_data/location/01.data/area.dart';

import '../../page/location/location_explore_rooms_page.dart';

class LocationExploreAreasView extends StatelessWidget {
  final List<Area> areas;
  const LocationExploreAreasView({required this.areas, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return areas == []
        ? Center(child: Text("There is no area yet!"))
        : Container(
            padding: EdgeInsets.symmetric(vertical: 12),
            child: Column(children: [
              Expanded(
                child: ListView.builder(
                  itemCount: areas.length,
                  itemBuilder: (context, index) => Card(
                    margin: EdgeInsets.all(8),
                    elevation: 4,
                    child: ListTile(
                        leading: Icon(
                          Icons.add_a_photo_sharp,
                          color: Theme.of(context).colorScheme.secondary,
                        ),
                        title: Text(
                          areas[index].title,
                          style: Theme.of(context).textTheme.headline6,
                        ),
                        tileColor: Theme.of(context).highlightColor,
                        onTap: () => Navigator.of(context).push<void>(
                              MaterialPageRoute(
                                  builder: (context) =>
                                      LocationExploreRoomsPage(areas[index])),
                            )),
                  ),
                ),
              )
            ]),
          );
  }
}
