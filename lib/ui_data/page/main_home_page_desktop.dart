import 'package:flutter/material.dart';

import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../logic_data/generalAppInfo/routing/home_page_routing_item.dart';

import '../widget/home_grid_tile.dart';

class MainHomePageDesktop extends ConsumerWidget {
  const MainHomePageDesktop(this.deviceInfo, {Key? key}) : super(key: key);
  final MediaQueryData deviceInfo;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    double width = deviceInfo.size.width;
    List<HomePageRoutingItem> itemList = ref.watch(getHomePageRoutingItemList);
    return SafeArea(
      //margin: EdgeInsets.all(24),

      child: Container(
        width: width < 400 ? width : 400 + (width - 400) / 3,
        margin: EdgeInsets.all(24),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: GridView.builder(
                    physics: BouncingScrollPhysics(),
                    padding: EdgeInsets.all(24),
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisSpacing: 10,
                        mainAxisSpacing: 10,
                        crossAxisCount: 2),
                    itemCount: itemList.length,
                    itemBuilder: (BuildContext context, int index) {
                      return HomeGridTile(
                        item: itemList[index],
                      );
                    }),
              )
            ]),
      ),
    );
  }
}
