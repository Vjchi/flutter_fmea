import 'package:flutter/material.dart';

import 'package:flutter_hooks/flutter_hooks.dart';

import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../../logic_data/generalAppInfo/theming/data/text_theme.dart';
import '../../logic_data/generalAppInfo/theming/data/color_theme.dart';
import '../../logic_data/generalAppInfo/theming/data/light_mode_theme.dart';
import '../../logic_data/generalAppInfo/theming/page/theme_page.dart';
import 'main_home_page_desktop.dart';

class MyHomePage extends HookConsumerWidget {
  final String title;
  MyHomePage({required this.title});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    MediaQueryData _deviceInfo; //The MediaQuery to use the size of the screen
    BuildContext _context = useContext();
    _deviceInfo = MediaQuery.of(_context);

    //Connect to LightMode Provider
    final _isLightMode = ref.watch(customLightThemeProvider.notifier);
    //Fetch the Light mode setting
    bool _isLightModeState = _isLightMode.getState();

    //Setup Accessible Text settign
    bool _isAccessibleText = false;

    return Scaffold(
      appBar: AppBar(
        title: Text(
          title,
        ),
      ),
      drawer: Drawer(
        elevation: 4.0,
        child: Column(
          children: [
            SizedBox(
              height: 24,
            ),
            Text("Options"),
            SizedBox(
              height: 12,
            ),

            //SwitchTile for Dark Mode
            SwitchListTile(
              value: _isLightModeState,
              title: Text("Theme Mode"),
              subtitle: Text("Change your light/dark mode here"),
              secondary: Icon(Icons.lightbulb_outline_rounded),
              onChanged: (newValue) {
                //onChanged fetches the requested Value, applies it to isLightMode
                _isLightMode.switchLightMode(newValue);
                //and updates the UI
              },
            ),
            SizedBox(height: 8),

            //List Tile to switch the ColorPalette
            ListTile(
              leading: Icon(Icons.change_circle_rounded),
              title: Text("Change Color Scheme"),
              onTap: () {
                ref.watch(customColorThemeProvider.notifier).switchColor();
              },
            ),
            SizedBox(
              height: 8,
            ),
            SwitchListTile(
                value: _isAccessibleText,
                secondary: Icon(Icons.format_size_outlined),
                title: Text("Accessible Text?"),
                onChanged: (value) {
                  _isAccessibleText = value;
                  ref
                      .watch(customTextThemeProvider.notifier)
                      .toggleAccessibleText(value);
                }),

            //Add a link for the setting page
            ListTile(
              leading: Icon(Icons.format_paint),
              title: Text("Settings"),
              onTap: () => Navigator.of(context).pushNamed(ThemePage.routeName),
            )
          ],
        ),
      ),
      body: LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
          return MainHomePageDesktop(_deviceInfo);
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          ref.watch(customColorThemeProvider.notifier).switchColor();
        },
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
    );
  }
}
