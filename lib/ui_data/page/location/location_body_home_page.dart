import 'package:flutter/material.dart';
import 'package:flutter_fmea/ui_data/page/location/location_explore_buildings_page.dart';

class LocationBodyHomePage extends StatelessWidget {
  LocationBodyHomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
          child: ListView(
            padding: EdgeInsets.all(12),
            physics: BouncingScrollPhysics(),
            children: [
              InkWell(
                onTap: () => ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(content: Text('You quickly pressed the button'))),
                onLongPress: () =>
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                  content: Text('You pressed the button for a long time'),
                  backgroundColor: Theme.of(context).primaryColorLight,
                )),
                child: Card(
                  elevation: 4,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: ListTile(
                    contentPadding: EdgeInsets.all(12),
                    subtitle: Text(
                      'Visit the different locations, and add some if necessary.',
                      maxLines: 3,
                    ),
                    title: Text(
                      'Explore',
                      style: Theme.of(context).textTheme.headline6,
                    ),
                    leading: Icon(
                      Icons.public,
                      size: 48,
                    ),
                    onTap: () => Navigator.of(context).pushNamed(LocationExploreBuildingsPage.routeName),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
