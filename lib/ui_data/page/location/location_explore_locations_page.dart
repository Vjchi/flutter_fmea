import 'package:flutter/material.dart';
import 'package:flutter_fmea/logic_data/location/02.use-case/rooms.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../../../logic_data/location/01.data/room.dart';
import '../../../logic_data/location/01.data/location.dart';
import '../../../logic_data/location/02.use-case/locations.dart';

class LocationExploreLocationsPage extends ConsumerWidget {
  final String roomID;
  LocationExploreLocationsPage({Key? key, required this.roomID})
      : super(key: key);

  static const String routeName = "LocationExploreLocationsPage";

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    //Fetch the room from the Provider
    RoomsState rooms = ref.watch(roomsProvider.notifier);
    Room room = rooms.getRooms.firstWhere((room) => room.id == roomID);

    //Fetch the locations associated to this room
    LocationsState locations = ref.watch(locationsProvider.notifier);
    List<Location> locationsList = locations.getLocations;

    return Scaffold(
      appBar: AppBar(
        title: Text("Explore ${room.title}"),
      ),
      body: Center(
        child: room.locationsIDs == null
            ? Column(
                children: [
                  CircularProgressIndicator(),
                  Text("No locations added yet")
                ],
              )
            : ListView.builder(
                itemCount: locationsList.length,
                itemBuilder: (BuildContext context, int index) {
                  return Card(
                    child: ListTile(
                      title: Text(room.locationsIDs![index]),
                    ),
                  );
                }),
      ),
    );
  }
}
