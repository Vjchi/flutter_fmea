import 'package:flutter/material.dart';

import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../../../logic_data/location/01.data/building.dart';
import '../../../logic_data/location/01.data/area.dart';
import '../../../logic_data/location/02.use-case/areas.dart';

import '../../../ui_data/widget/location/location_explore_areas_view.dart';

class LocationExploreAreas extends HookConsumerWidget {
  LocationExploreAreas({required this.building, Key? key}) : super(key: key);
  final Building building;
  static const String routeName = "LocationExploreAreasPage";

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    //Start by initializing the controllers, and parameters
    final TabController _tabController =
        useTabController(initialLength: building.floors.length);

    final List<String> _tabNameList;
    final List<Tab> _tabList;
    List<Widget> _tabViews = [];

    //Create boolean to manage if there are areas or not
    bool _isArea = true;

    //Access Riverpod provider:
    final AreasState _areasState = ref.watch(areasProvider.notifier);

    //Check if there are areas
    final List<Area>? _buildingAreasList = _areasState.getAreas(building.id);
    _buildingAreasList == [] ? _isArea = false : _isArea = true;

    //Only generate the data if there are areas
    if (_isArea) {
      //Generate the tab names list
      _tabNameList = building.getFloorList();
      //Create the list of Tabs
      _tabList = [];
      for (String name in _tabNameList) {
        _tabList.add(Tab(
          child: Text(
            name,
            style: Theme.of(context).textTheme.headline5,
          ),
        ));
      }
      //Create the list of TabView
      for (int floorIndex in building.floors) {
        //Add the widget view
        _tabViews.add(LocationExploreAreasView(
            areas: _buildingAreasList
                    ?.where((element) => element.floor == floorIndex)
                    .toList() ??
                []));
      }
    } else {
      _tabList = [];
      _tabViews = [];
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Explore Areas",
        ),
      ),
      body: Container(
        child: Column(
          children: [
            Container(
              alignment: Alignment.centerLeft,
              child: Text(
                building.title,
                maxLines: 1,
                overflow: TextOverflow.clip,
                style: Theme.of(context).textTheme.headline4,
              ),
            ),
            Container(
              alignment: Alignment.centerLeft,
              margin: EdgeInsets.symmetric(vertical: 12),
              child: TabBar(
                tabs: _tabList,
                controller: _tabController,
                isScrollable: true,
                indicatorColor: Theme.of(context).colorScheme.primary,
              ),
            ),
            Expanded(
              child: TabBarView(
                controller: _tabController,
                children: _tabViews,
              ),
            ),
            Text(
              "TEST UI",
              style: Theme.of(context).textTheme.headline3,
            )
          ],
        ),
      ),
    );
  }
}
