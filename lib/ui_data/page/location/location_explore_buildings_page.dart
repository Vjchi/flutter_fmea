import 'package:flutter/material.dart';

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../../../logic_data/location/01.data/building.dart';
import '../../../logic_data/location/02.use-case/buildings.dart';

import '../../widget/location/new_building_bottom_sheet.dart';
import '../../../ui_data/page/location/location_explore_areas_page.dart';

class LocationExploreBuildingsPage extends ConsumerWidget {
  static const String routeName = "LocationExploreBuildingsPage";

  Widget build(BuildContext context, WidgetRef ref) {
    final BuildingsState _buildingsState =
        ref.watch(buildingsProvider.notifier);
    final List<Building> _buildingsList = ref.watch(buildingsProvider);

    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Explore Buildings",
        ),
      ),
      body: SafeArea(
        child: Padding(
            padding: EdgeInsets.all(20),
            child: Column(
              children: [
                Text(
                  "Choose your building:",
                  style: Theme.of(context).textTheme.headline5,
                ),
                SizedBox(
                  height: 12,
                ),
                Expanded(
                  child: ListView.builder(
                      itemCount: _buildingsList.length,
                      itemBuilder: (BuildContext context, int index) {
                        return ListTile(
                            title: Text(_buildingsList[index].title),
                            leading: Icon(Icons.store),
                            trailing: IconButton(
                              icon: Icon(Icons.delete_forever_rounded),
                              onPressed: () => _buildingsState
                                  .removeBuilding(_buildingsList[index].id),
                            ),
                            onTap: () => Navigator.of(context).push<void>(
                                  MaterialPageRoute(
                                    builder: (context) => LocationExploreAreas(
                                      building: _buildingsList[index],
                                    ),
                                  ),
                                ));
                      }),
                )
              ],
            )),
      ),
      floatingActionButton: Builder(builder: (BuildContext context) {
        return FloatingActionButton(
            child: Icon(Icons.add),
            tooltip: "Add a building",
            onPressed: () {
              print("Trying to add a building");
              ScaffoldMessenger.of(context).removeCurrentSnackBar();
              showDialog(
                  barrierDismissible: true,
                  context: context,
                  builder: (context) => Dialog(
                        child: NewBuildingBottomSheet(),
                        elevation: 0,
                        backgroundColor: Colors.transparent,
                      ));
            });
      }),
    );
  }
}
