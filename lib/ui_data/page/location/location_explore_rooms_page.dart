import 'package:flutter/material.dart';

import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../../../logic_data/location/01.data/area.dart';
import '../../../logic_data/location/01.data/room.dart';

import '../../../logic_data/location/02.use-case/rooms.dart';

import './location_explore_locations_page.dart';

class LocationExploreRoomsPage extends HookConsumerWidget {
  final Area area;
  const LocationExploreRoomsPage(this.area, {Key? key}) : super(key: key);
  //static const routeName = "LocationExploreRoomsPage";

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final List<Room>? _rooms = ref.watch(roomsProvider);

    return Scaffold(
      appBar: AppBar(
        title: Text('Explore Rooms'),
      ),
      body: Container(
        child: Column(
          children: [
            area.rooms == null
                ? Padding(
                    padding: const EdgeInsets.all(24),
                    child: Text(
                      "There is no room yet. Create one !",
                      style: Theme.of(context).textTheme.headline4,
                      textAlign: TextAlign.center,
                    ),
                  )
                : Expanded(
                    child: ListView.builder(
                      itemCount: area.rooms!.length,
                      itemBuilder: (context, index) => Card(
                        margin: EdgeInsets.all(8),
                        elevation: 4,
                        child: ListTile(
                          leading: Icon(
                            Icons.add_a_photo_sharp,
                            color: Theme.of(context).colorScheme.secondary,
                          ),
                          title: Text(
                            _rooms![index].title,
                            style: Theme.of(context).textTheme.headline6,
                          ),
                          tileColor: Theme.of(context).highlightColor,
                          onTap: () =>
                              Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => LocationExploreLocationsPage(
                                roomID: area.rooms![index]),
                          )),
                        ),
                      ),
                    ),
                  ),
            Center(
              child: Padding(
                padding: const EdgeInsets.all(42),
                child: CircularProgressIndicator(
                  color: Theme.of(context).colorScheme.secondary,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
