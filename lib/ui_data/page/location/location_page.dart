import 'package:flutter/material.dart';
import 'location_body_home_page.dart';

class LocationHomePage extends StatelessWidget {
  const LocationHomePage({Key? key}) : super(key: key);
  static const routeName = 'LocationPage';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Locations"),
      ),
      body: Center(
        child: LocationBodyHomePage(),
      ),
    );
  }
}
